<?php

namespace Drupal\rfn_sharing\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;

/**
 * Returns responses for RFN Sharing routes.
 */
class RfnSharingController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {

    $host = \Drupal::request()->getSchemeAndHttpHost();
    $referrer = str_replace($host, '', $_SERVER['HTTP_REFERER']);

    $path = \Drupal::service('path.alias_manager')->getPathByAlias($referrer);
    if (preg_match('/node\/(\d+)/', $path, $matches)) {
      $node = Node::load($matches[1]);
    }

    $sharingMarkup = $this->buildSharingMarkup($node->getTitle(), $_SERVER['HTTP_REFERER']);

    $options = [
      'dialogClass' => 'popup-dialog-class',
      'width' => '50%',
    ];
    $response = new AjaxResponse();
    $response->addCommand(new OpenModalDialogCommand(t('Sharing is caring'), $sharingMarkup, $options));
    $response->addAttachments(['library' => 'rfn_sharing/rfn_sharing']);
    return $response;
  }

  /**
   * Build the markup for the sharing modal.
   */
  private static function buildSharingMarkup($title, $url) {

    $iconPath = '/' . drupal_get_path('module', 'rfn_sharing') . '/icons/';
    $shareTo = t('Share to');
    $title = urlencode($title);

    // Are we sharing a track?  If so, append that to the url.
    $fullUri =  $_SERVER['REQUEST_URI'];
    
    if(stristr($fullUri, 'track=')) {
      $matches = [];
      preg_match("/track=([0-9]+)/", $fullUri, $matches);
      $url .= '?track=' . $matches[1];
    }

    // Facebook.
    $output .= <<< EOD
    <div class="rfn-sharing-button">
        <a href="https://www.facebook.com/sharer/sharer.php?u=$url&title=$title" target="_blank" title="$shareTo Facebook" aria-label="$shareTo Facebook" style="width: 30px; height: 30px;">
            <img src="$iconPath/facebook.svg" title="$shareTo Facebook" style="width: 30px; height: 30px;"/>
        </a>
    </div>
EOD;

    // Twitter.
    $output .= <<< EOD
    <div class="rfn-sharing-button">
      <a href="http://twitter.com/intent/tweet?text=$title+$url" target="_blank" title="$shareTo} Twitter" aria-label="$shareTo Twitter" class="social-sharing-buttons__button" rel="noopener" style="width: 30px; height: 30px;">
      <img src="$iconPath/twitter.svg" title="$shareTo Twitter" style="width: 30px; height: 30px;"/>
      </a>
    </div>
EOD;

    // What's App.
    $output .= <<< EOD
    <div class="rfn-sharing-button">
    <a href="https://wa.me/?text=$url" target="_blank" title="$shareTo WhatsApp" aria-label="$shareTo WhatsApp" class="social-sharing-buttons__button"
       rel="noopener">
      <img src="$iconPath/whatsapp.svg" title="$shareTo Whatsapp" style="width: 30px; height: 30px;"/>
    </a>
    </div>
EOD;

    // Reddit.
    $output .= <<< EOD
    <div class="rfn-sharing-button">
    <a href="http://www.reddit.com/submit?url=$url&title=$title"
 target="_blank" title="$shareTo Reddit" aria-label="$shareTo Reddit" class
="social-sharing-buttons__button" rel="noopener">
      <img src="$iconPath/reddit.svg" title="$shareTo Reddit" style="width: 30px; height: 30px;"/>
        </a>
    </div>
EOD;

    // Email.
    $siteName = \Drupal::config('system.site')->get('name');
    $sharedFrom = t('Sharing a link from');
    $sharedFrom .= ' ' . $siteName;
    
    $output .= <<< EOD
      <div class="rfn-sharing-button">
        <a href="mailto:?subject=$sharedFrom: $title&body=$sharedFrom: $title -- $url"
target="_blank" title="$shareTo Email" aria-label="$shareTo Email" class
="social-sharing-buttons__button" rel="noopener">
          <img src="$iconPath/email.svg" title="$shareTo Email" style="width: 30px; height: 30px;"/>
      </a>
  </div>
EOD;

    $output .= <<< EOD
          <div class="rfn-sharing-button">
            <a onclick="copyUrl('$url');" style="cursor: pointer;"
    target="_blank" title="Copy" aria-label="Copy" class
    ="social-sharing-buttons__button" rel="noopener">
              <img src="$iconPath/copy.svg" title="Copy" style="width: 30px; height: 30px;"/>
          </a>
      </div>
    EOD;
    return $output;
  }

}
