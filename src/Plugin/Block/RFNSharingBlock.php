<?php

namespace Drupal\rfn_sharing\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Render\Markup;

/**
 * Provides sharing buttons for RFN sites.
 *
 * @Block(
 *   id = "rfn_sharing_buttons",
 *   admin_label = @Translation("RFN Sharing Buttons"),
 *   category = @Translation("RFN Sharing")
 * )
 */
class RFNSharingBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    $node = \Drupal::routeMatch()->getParameter('node');
    $url = $host = \Drupal::request()->getSchemeAndHttpHost() . $node->toUrl()->toString();

    $build['content'] = [
      '#markup' => $this->buildSharing($node->getTitle(), $url),
    ];
    return $build;
  }

  /**
   * Build the sharing buttons.
   */
  private function buildSharing($title, $url) {

    $link_url = Url::fromRoute('rfn_sharing.modal');

    $link_url->setOptions([
      'attributes' => [
        'class' => ['use-ajax', 'button', 'button--small'],
        'data-dialog-type' => 'modal',
        'data-dialog-options' => "width: 400",
        'title' => t('Share this'),
      ],
    ]);

    $img = '<img src="' . base_path() . drupal_get_path('module', 'rfn_sharing') . '/icons/share.svg" style="width: 20px; height: 20px;"/>';
    $rendered_image = render($img);
    $image_markup = Markup::create($rendered_image);

    $data = [
      '#type' => 'markup',
      '#markup' => Link::fromTextAndUrl($image_markup, $link_url)->toString(),
      '#attached' => ['library' => ['core/drupal.dialog.ajax']],
    ];

    $rendered = \Drupal::service('renderer')->renderPlain($data);
    return $rendered;
  }

}
